﻿using System;

namespace TableNet
{
    public class Cell
    {
        public Row Row { get; set; } = null;
        public Column Column { get; set; } = null;

        public string Text { get; set; } = "";

        public char Filler { get; set; } = ' ';

        public Cell() { }

        public Cell(string text)
        {
            Text = text;
        }
    }
}
