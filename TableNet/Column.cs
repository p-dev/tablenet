﻿using System;
using System.Collections.Generic;
namespace TableNet
{
    public class Column
    {
        public List<Cell> Cells { get; set; }

        public int Width { get; set; }

        public char Filler { set => Cells.ForEach(c => c.Filler = value); }

        public Column() { }

        public Column(int width)
        {
            Width = width;
        }
    }
}
