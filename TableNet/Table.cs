﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace TableNet
{
    public class Table
    {
        private Cell[,] _cells;
     
        public Cell this[int i, int j]
        {
            get => _cells[i, j];
            set
            {
                _cells[i, j] = value;
                Rows[i].Cells[j] = value;
                Columns[j].Cells[i] = value;
            }
        }


        public Row[] Rows { get; }
        public Column[] Columns { get; }

        public Table(Column[] columns, Row[] rows)
        {
            Rows = rows;
            Columns = columns;

            _cells = new Cell[Rows.Length, Columns.Length];

            for (int i = 0; i < Rows.Length; i++)
            {
                for (int j = 0; j < Columns.Length; j++)
                {
                    Rows[i].Cells.Add(_cells[i, j]);
                    Columns[j].Cells.Add(_cells[i, j]);
                }
            }
        }

        public Table(Column[] columns, Row[] rows, Cell[,] cells)
        {
            if (cells.GetLength(0) != rows.Length ||
                cells.GetLength(1) != columns.Length) throw new ArgumentException("Rows and Columns sizes not equals with Cells sizes.");
        
            Rows = rows;
            Columns = columns;

            _cells = cells;
        }

        public char[,] GetCharMatrix()
        {
            // Create char matrix
            var cmat = new char[
                (Rows   .Select(r => r.Height).Sum() + Rows.Length    + 1), 
                (Columns.Select(c => c.Width) .Sum() + Columns.Length + 1)
                ];
            // Fill it with '.'
            for (int i = 0; i < cmat.GetLength(0); i++)
                for (int j = 0; j < cmat.GetLength(1); j++)
                    cmat[i, j] = '.';

            // Render table borders
            
            // Get places where is border lines
            var hLines = new List<int>() { 0 };
            for (int i = 0; i < Rows.Length; i++)
                hLines.Add(hLines[i] + Rows[i].Height + 1);
            var vLines = new List<int>() { 0 };
            for (int j = 0; j < Columns.Length; j++)
                vLines.Add(vLines[j] + Columns[j].Width + 1);

            // Set V/H lines
            foreach (var i in hLines)
                for (int j = 0; j < cmat.GetLength(1); j++)
                    cmat[i, j] = Border.H;
            foreach (var j in vLines)
                for (int i = 0; i < cmat.GetLength(0); i++)
                    cmat[i, j] = Border.V;

            // Set crosses
            foreach (var i in hLines)
                foreach (var j in vLines)
                {
                    if (i == 0)
                        cmat[i, j] = Border.HD;
                    else if (i == cmat.GetLength(0) - 1)
                        cmat[i, j] = Border.HU;
                    else if (j == 0)
                        cmat[i, j] = Border.VR;
                    else if (j == cmat.GetLength(1) - 1)
                        cmat[i, j] = Border.VL;
                    else
                        cmat[i, j] = Border.VH;
                }

            // Set angles
            cmat[0                     , 0]                     = Border.DR;
            cmat[0                     , cmat.GetLength(1) - 1] = Border.DL;
            cmat[cmat.GetLength(0) - 1 , 0]                     = Border.UR;
            cmat[cmat.GetLength(0) - 1 , cmat.GetLength(1) - 1] = Border.UL;


            // Render text

            for (int i = 0; i < Rows.Length; i++)
            {
                for (int j = 0; j < Columns.Length; j++)
                {
                    var row = Rows[i];
                    var column = Columns[j];
                    var cell = _cells[i, j];
                    
                    var yCell = Rows   .Take(i).Select(r => r.Height).Sum() + i + 1;
                    var xCell = Columns.Take(j).Select(r => r.Width) .Sum() + j + 1;

                    for (int y = 0; y < row.Height; y++)
                    {
                        for (int x = 0; x < column.Width; x++)
                        {
                            if (y * column.Width + x < cell.Text.Length)
                                cmat[yCell + y, xCell + x] = cell.Text[y * column.Width + x];
                            else
                                cmat[yCell + y, xCell + x] = cell.Filler;
                        }
                    }
                }
            }

            return cmat;
        }

        public override string ToString()
        {
            var cmat = this.GetCharMatrix();
            
            // Char matrix to string

            var sb = new StringBuilder(cmat.Length + cmat.GetLength(0) - 1);

            for (int i = 0; i < cmat.GetLength(0); i++)
            {
                for (int j = 0; j < cmat.GetLength(1); j++)
                {
                    sb.Append(cmat[i, j]);
                }
                sb.Append('\n');
            }

            return sb.ToString();
        } 
    }
}
