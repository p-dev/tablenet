﻿using System;
namespace TableNet
{
    public static class Border
    {
        public const char H = '─';
        public const char V = '│';
        public const char DR = '┌';
        public const char DL = '┐';
        public const char UR = '└';
        public const char UL = '┘';
        public const char VR = '├';
        public const char VL = '┤';
        public const char HD = '┬';
        public const char HU = '┴';
        public const char VH = '┼';
    }
}
