﻿using System;
using System.Collections.Generic;

namespace TableNet
{
    public class Row
    {
        public List<Cell> Cells { get; set; }

        public int Height { get; set; }

        public char Filler { set => Cells.ForEach(c => c.Filler = value); }

        public Row() { }

        public Row(int height)
        {
            Height = height;
        }
    }
}
