using System;
using Xunit;
using TableNet;

namespace TableNetXUnit
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var table = new Table(
                new Column[] { new Column(5), new Column(7), new Column(6), },
                new Row[]
                { 
                    new Row(3),
                    new Row(2),
                },
                new Cell[,]
                {
                    { new Cell("Some text"), new Cell("Another text"), new Cell() },
                    { new Cell("A very big text which not blablabla"), new Cell("ltl txt"), new Cell("aaaa") },
                }
            );

            Console.WriteLine(table.ToString());
        }
    }
}
